# Film Mapping Ontology

This ontology including schema is used for a pilot project that aims to map
films, their locations and their scenes.

## Credits

Film Mapping Idea by Erik Champion, Film Mapping Data Model by Tristan Reed,
Film Mapping Operationalising byMatt Lavender.
